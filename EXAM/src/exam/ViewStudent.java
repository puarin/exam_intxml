/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Puuuuu
 */
public class ViewStudent {
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        ArrayList<Student> st = new ArrayList<>();
        st = view();
         for (Student student : st) {
            System.out.println(student.getId());
            System.out.println(student.getName());
            System.out.println(student.getAddress());
    }
    }
      public static ArrayList<Student> view() throws ParserConfigurationException, SAXException, IOException {
        File f = new File("src/exam/students.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder buider = factory.newDocumentBuilder();
        Document doc = buider.parse(f);
        Element students = doc.getDocumentElement();
        NodeList studentList = students.getElementsByTagName("student");
        ArrayList<Student> st = new ArrayList<>();
        for (int i = 0; i < studentList.getLength(); i++) {
            Node node = studentList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element student = (Element) node;
                int id = Integer.parseInt(student.getElementsByTagName("id").item(0).getTextContent());
                String name = student.getElementsByTagName("name").item(0).getTextContent();
                String address = student.getElementsByTagName("address").item(0).getTextContent();
                Student c = new Student();
                c.setId(id);
                c.setName(name);
                c.setAddress(address);
                st.add(c);
            }
        }
        return st;
    }
}
