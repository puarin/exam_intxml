/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.ValidityException;
import nux.xom.xquery.XQueryUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
/**
 *
 * @author Puuuuu
 */
public class Searchid {
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, ParsingException {
        ArrayList<Student> st = new ArrayList<>();
        st = search(1);
        for (Student student : st) {
            System.out.println(student.getId());
            System.out.println(student.getName());
            System.out.println(student.getAddress());
        }
    }
 public static ArrayList<Student> search(int id) throws ParsingException, ValidityException, IOException {
        nu.xom.Document document = new nu.xom.Builder().build(new File("src/exam/students.xml"));    
        nu.xom.Nodes nodes = XQueryUtil.xquery(document, "//student[id=" + id + "]");
        ArrayList<Student> arrayList = new ArrayList<>();
        for (int i = 0; i < nodes.size(); i++) {
            Student pro = new Student();
            nu.xom.Node node = nodes.get(i);
            pro.setId(Integer.parseInt(node.getChild(1).getValue()));
            pro.setName(node.getChild(3).getValue());
            pro.setAddress(node.getChild(5).getValue());
           
          
            arrayList.add(pro);
        }

        return arrayList;
    }

}
