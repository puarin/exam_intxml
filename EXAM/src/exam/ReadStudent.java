/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Puuuuu
 */
public class ReadStudent {
    public static void main(String[] args) {
        
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new File("src/exam/students.xml"));
            ReadStudent std = new ReadStudent();
            doc.getDocumentElement().normalize();
         System.out.println("Student :" + doc.getDocumentElement().getNodeName());
			
	NodeList nList = doc.getElementsByTagName("student");
        System.out.println("----------------------------");

	for (int temp = 0; temp < nList.getLength(); temp++) {

		Node nNode = nList.item(temp);
				
		System.out.println("\n " + nNode.getNodeName());
				
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {

			Element eElement = (Element) nNode;

			System.out.println("id : " + eElement.getElementsByTagName("id").item(0).getTextContent());
			System.out.println("Name : " + eElement.getElementsByTagName("name").item(0).getTextContent());
			System.out.println("Address : " + eElement.getElementsByTagName("address").item(0).getTextContent());
                }
        }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ReadStudent.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(ReadStudent.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadStudent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
